package pl.sda.zmienne;


import java.util.InputMismatchException;
import java.util.Scanner;

public class InputOutputExamples {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Podaj swoje imię: ");

        //String name = in.next(); //metoda next() zwraca pierwszy wpisany przez użytkownika wyraz

        String name = in.nextLine(); //metoda nextLine() zwraca cały wpisany przez użytkownika ciąg znaków


        System.out.println("Witaj, " + name);

        System.out.println("Podaj swój wiek: ");

        int age;

        try {
            age = in.nextInt();
        } catch (InputMismatchException e) {
            age = -1;
        }

        if (age >= 0 && age < 18) {
            System.out.println("Jesteś niepełnoletni");
        } else if (age >= 18) {
            System.out.println("Jesteś pełnoletni");
        } else {
            System.out.println("Podano nieprawidłową wartość");
        }


        System.out.println("Podaj miesiąc urodzenia: ");
        int month = in.nextInt();

        String text = "Urodziłeś się w miesiącu ";

        switch (month) {
            case 1:
                System.out.println(text + "styczniu"); break;
            case 2:
                System.out.println(text + "lutym"); break;
            case 3:
                System.out.println(text + "marcu"); break;
            case 4:
                System.out.println(text + "kwietniu"); break;
            default:
                System.out.println("Nieobsługiwana wartość"); break;
        }

    }

}
