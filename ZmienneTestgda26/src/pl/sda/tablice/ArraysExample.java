package pl.sda.tablice;

public class ArraysExample {

    public static void main(String[] args) {

        int [] array = new int[5];
        System.out.println("Długość tablicy array: " + array.length);
        System.out.println("Zawartość komórki o indeksie 0: " + array[0]);

        array[0] = 7;
        System.out.println("Zawartość komórki o indeksie 0 po aktualizacji: " + array[0]);

        int [] array2 = new int[]{1, 3, 6, 8, 12, 43};
        System.out.println("Długość tablicy array2: " + array2.length);

        for (int i=0; i < array2.length; i++) {
//            array2[i] = array2[i] * 3;
            //ten kod jest równy powyższemu
            array2[i] *= 3;
            System.out.print(array2[i] + " ");
        }

        //System.out.println(array2[6]); //uruchomienie tej instrukcji spowoduje wyjście poza zakres tablicy array2

        for (int i=0; i<array2.length; i++) {

            if ( array2[i] % 2 == 0) { //operator % (modulo) zwraca resztę z dzielenia
                //System.out.println("Element " + array2[i] + " jest parzysty");
                //wywołanie metody printf działa tak samo jak kod powyżej
                System.out.printf("Element %d jest parzysty\n", array2[i]);

            } else {
                //System.out.println("Element " + array2[i] + " jest nieparzysty");
                //wywołanie metody printf działa tak samo jak kod powyżej
                System.out.printf("Element %d jest nieparzysty\n", array2[i]);
            }
        }




    }

}
