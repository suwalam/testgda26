package pl.sda.petle;

public class ForExample {

    public static void main(String[] args) {


        for (int i = 0; i < 8; i++){
            System.out.print(i + " ");
        }

        //***************************
        System.out.println(); //wypisanie znaku nowej linii
        int i2 = 0;
        while (i2 < 8) {
            System.out.print(i2 + " ");
            i2++;
        }

        //***************************
        System.out.println();
        int i3 = 0;
        do {
            System.out.print(i3 + " ");
            i3++;
        } while (i3 < 8);


    }

}
