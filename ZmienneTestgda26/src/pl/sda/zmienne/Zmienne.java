package pl.sda.zmienne;

public class Zmienne {

    // Dowolny tekst będący komentarzem

    /*
    Komentarz wielolinijkowy
    Linia druga
     */

    //ctrl + / - komentarz
    //ctrl + Shift + / - komentarz wielolinijkowy

    public static void main(String[] args) {

        System.out.println("Hello World!");

        byte byteVar = 100; //zakres -128 - 127

        System.out.println("Wartość zmiennej byteVar wynosi " + byteVar);

        byteVar = (byte) 140; //inr można przypisać int do byte

        System.out.println("Wartość zmiennej byteVar po aktualizacji wynosi " + byteVar);

        int intVar = 1000000000;

        System.out.println("Wartość zmiennej intVar wynosi " + intVar);

        intVar = byteVar; //po lewej stronie przypisania znajduje się zmienna o większym zakresie niż po prawo

        long longVar = 100000000000L;

        float floatVar = 3.9999999F;

        double doubleVar = 235234.77777563563546;

        System.out.println("Wynik mnożenia: " + floatVar * 3);

        intVar = (int ) doubleVar; //wszystko co znajduje się po prawej stronie kropki jest usuwane

        System.out.println("Wartość zmiennej intVar po aktualizacji wynosi " + intVar);

        boolean boolVar = false;
        boolVar = true;

        char charVar = '@';
        charVar = 100;

        System.out.println("Wartość zmiennej charVar wynosi " + charVar);

        String strVar = "dowolny napis";
    }


}



