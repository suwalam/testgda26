package pl.sda.zmienne;

import java.math.BigDecimal;
import java.math.BigInteger;

public class BigDecimalExample {

    public static void main(String[] args) {

        BigDecimal dec1 =
                new BigDecimal("3457645745746747467467467467467467.999999999999999999999999999999999999999999");

        dec1 = dec1.multiply(new BigDecimal("3578758675867876876868678678678768786578768768768"));

        System.out.println(dec1);

        BigInteger int1 = new BigInteger("134235235245145631463163163163263163635636351111");
        int1 = int1.multiply(new BigInteger("35463456356363636363532635634636356356"));
        System.out.println(int1);


    }

}
