package pl.sda.obiekty;

public class DogMain {

    public static void main(String[] args) {

        Dog dog1 = new Dog("Owczarek niemiecki", "Reksio", 13.67);

        Dog dog2 = new Dog("Kundel", "Misiek", 5.43);

        dog1.bark();
        dog2.bark();

        System.out.println(dog1.fetch("kość"));

        System.out.printf("Pies o nazwie %s jest rasy %s i waży %f kg\n",
                dog1.getName(), dog1.getBreed(), dog1.getWeight());

        dog2.setWeight(12.4);
        dog2.bark();

    }

}
