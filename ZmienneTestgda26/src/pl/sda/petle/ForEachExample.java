package pl.sda.petle;

public class ForEachExample {

    public static void main(String[] args) {

        int [] array = new int[]{4, 7, 10};

        for (int element : array) {
            System.out.print(element + " ");
        }

        System.out.println("\nPrzed aktualizacją");
        for (int element : array) {
            element = element * 3;
            System.out.print(element + " ");
        }

        System.out.println("\nPo aktualizacji");

        for (int element : array) {
            System.out.print(element + " ");
        }

    }

}
