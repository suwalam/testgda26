package pl.sda.petle;

public class WhileExample {

    public static void main(String[] args) {

        boolean myFlag = true;
        int iter = 0;

        while (myFlag) {
            System.out.print(iter++);

            if (iter == 5) {
                myFlag = false;
            }

            System.out.print("x");
        }
    }

}
