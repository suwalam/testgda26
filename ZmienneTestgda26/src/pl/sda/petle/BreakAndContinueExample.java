package pl.sda.petle;

public class BreakAndContinueExample {

    public static void main(String[] args) {

        for (int i = 0; i < 8; ++i) {
            System.out.print(i);

            if (i == 3) {
                continue; //przerywa działanie bieżącej interacji
            }

            if (i == 5)  {
                break; //przerywa działanie całej pętli
            }

            System.out.print("x");
        }
    }
}
