package pl.sda.obiekty;

public class Dog {

    private String breed;

    private String name;

    private double weight;

    public Dog(String breed, String name, double weight) {
        this.breed = breed;
        this.name = name;
        this.weight = weight;
    }

    public String getBreed() {
        return breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null && !name.isEmpty()) {
            this.name = name;
        }
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        if (weight > 0) {
            this.weight = weight;
        }
    }

    public void bark() {
        if (weight < 10) {
            System.out.println("HAU HAU");
        } else {
            System.out.println("WOW WOW");
        }
    }

    public String fetch(String element) {
        return "FETCH " + element;
    }
}
